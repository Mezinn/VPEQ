<?php

use yii\db\Migration;

/**
 * Class m180506_081206_data
 */
class m180506_081206_data extends Migration {

    public function up() {
        return Yii::$app->db->createCommand(
                        "CREATE TABLE IF NOT EXISTS data(
                        id int AUTO_INCREMENT,
                        city varchar(255) NOT NULL,
                        company varchar(255) NOT NULL,
                        nsp varchar(255) NOT NULL,
                        quality_state int DEFAULT 0,
                        elapsed_time int DEFAULT 0,
                        productivity_level int DEFAULT 0,
                        difficulty_level int DEFAULT 0,
                        quality_index int DEFAULT 0,
                        created datetime,
                        updated datetime,
                        PRIMARY KEY (id)
                        )")->execute();
    }

    public function down() {
        return $this->dropTable("data");
    }

}
