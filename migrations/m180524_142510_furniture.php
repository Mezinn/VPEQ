<?php

use yii\db\Migration;

/**
 * Class m180524_142510_furniture
 */
class m180524_142510_furniture extends Migration
{
    
    public function up(){
        return $this->createTable('furnitures', [
           'uid'=> $this->primaryKey(),
            'name'=>$this->string(),
            'price'=>$this->decimal(),
            'type_uid'=>$this->integer(),
        ]);
    }
    
    public function down(){
        return $this->dropTable('furnitures');
    }
}
