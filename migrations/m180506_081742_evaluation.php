<?php

use yii\db\Migration;

/**
 * Class m180506_081742_evaluation
 */
class m180506_081742_evaluation extends Migration {

    public function up() {
        return Yii::$app->db->createCommand(
                        "CREATE TABLE IF NOT EXISTS evaluation(
                        data_id int NOT NULL,
                        value decimal NOT NULL,
                        internal_index int DEFAULT 0
                        )")->execute();
    }

    public function down() {
        return $this->dropTable('evaluation');
    }

}
