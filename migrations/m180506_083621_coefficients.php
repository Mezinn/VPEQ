<?php

use yii\db\Migration;

/**
 * Class m180506_083621_coefficients
 */
class m180506_083621_coefficients extends Migration {

    public function up() {
        return Yii::$app->db->createCommand(
                        "CREATE TABLE IF NOT EXISTS coefficients(
                id int AUTO_INCREMENT,
                value decimal NOT NULL,
                PRIMARY KEY (id)
                )")->execute();
    }

    public function down() {
        return $this->dropTable("coefficients");
    }

}
