<?php

use yii\db\Migration;

/**
 * Class m180524_142523_furniture_type
 */
class m180524_142523_furniture_type extends Migration {

    public function up() {
        return $this->createTable('furniture_type', [
                    'uid' => $this->primaryKey(),
                    'name' => $this->string(),
        ]);
    }

    public function down() {
        return $this->dropTable('furniture_type');
    }

}
