<?php

namespace app\controllers;

use Yii;
use app\models\Data;
use yii\web\Controller;
use app\models\Evaluation;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\Coefficients;
use app\models\SelfEvaluation;
use yii\data\ArrayDataProvider;
use app\models\search\SearchData;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * DataController implements the CRUD actions for Data model.
 */
class DataController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Data models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new SearchData();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Data model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Data model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Data();

        $evaluations = ArrayHelper::getColumn(Coefficients::find()->all(), function() {
                    return new Evaluation(['data_id' => Data::find()->max('id') + 1]);
                });
        $selfEvaluations = ArrayHelper::getColumn(Coefficients::find()->all(), function() {
                    return new SelfEvaluation(['data_id' => Data::find()->max('id') + 1]);
                });

        $evaluationDataProvider = new ArrayDataProvider([
            'allModels' => $evaluations,
        ]);

        $selfEvaluationDataProvider = new ArrayDataProvider([
            'allModels' => $selfEvaluations,
        ]);

        if ($model->load(Yii::$app->request->post()) &&
                $model->validate() &&
                Evaluation::loadMultiple($evaluations, Yii::$app->request->post()) &&
                Evaluation::validateMultiple($evaluations) &&
                SelfEvaluation::loadMultiple($selfEvaluations, Yii::$app->request->post()) &&
                SelfEvaluation::validateMultiple($selfEvaluations)) {
            $model->save();
            foreach ($evaluations as $evaluation) {
                $evaluation->save();
            }
            foreach ($selfEvaluations as $evaluation) {
                $evaluation->save();
            }
            return $this->redirect(['index']);
        }
        return $this->render('create', compact('model', 'evaluationDataProvider', 'selfEvaluationDataProvider'));
    }

    /**
     * Updates an existing Data model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        $coeffCount = Coefficients::find()->count();
        $evaluations = Evaluation::find()->where(['data_id' => $model->id])->all();
        $selfEvaluations = SelfEvaluation::find()->where(['data_id' => $model->id])->all();
        for ($i = count($evaluations); $i < $coeffCount; $i++) {
            array_push($evaluations, new Evaluation(['data_id' => $model->id]));
        }

        for ($i = count($selfEvaluations); $i < $coeffCount; $i++) {
            array_push($selfEvaluations, new SelfEvaluation(['data_id' => $model->id]));
        }


        $evaluationDataProvider = new ArrayDataProvider([
            'allModels' => $evaluations,
        ]);
        $selfEvaluationDataProvider = new ArrayDataProvider([
            'allModels' => $selfEvaluations,
        ]);

        if ($model->load(Yii::$app->request->post()) &&
                $model->validate() &&
                Evaluation::loadMultiple($evaluations, Yii::$app->request->post()) &&
                Evaluation::validateMultiple($evaluations) &&
                SelfEvaluation::loadMultiple($selfEvaluations, Yii::$app->request->post()) &&
                SelfEvaluation::validateMultiple($selfEvaluations)) {
            $model->save();
            foreach ($evaluations as $evaluation) {
                $evaluation->save();
            }
            foreach ($selfEvaluations as $evaluation) {
                $evaluation->save();
            }
            return $this->redirect(['index']);
        }
        return $this->render('update', compact('model', 'evaluationDataProvider', 'selfEvaluationDataProvider'));
    }

    /**
     * Deletes an existing Data model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Data model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Data the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Data::findOne($id)) !== null) {
            $coeffCount = (Coefficients::find()->count()) - 1;
            Evaluation::deleteAll("internal_index > $coeffCount");
            SelfEvaluation::deleteAll("internal_index > $coeffCount");
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
