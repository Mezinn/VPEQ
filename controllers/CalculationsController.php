<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

use app\models\Data;
use yii\web\Controller;
use app\models\Coefficients;
use app\models\SelfEvaluation;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use app\models\Evaluation;
use app\models\EstimatesIntersection;

/**
 * Description of CalculationsController
 *
 * @author mezinn
 */
class CalculationsController extends Controller {

    public function actionIndex() {

        $data = Data::find()->all();





        $dataProvider = new ArrayDataProvider([
            'allModels' => $data
        ]);

        return $this->render('index', compact('dataProvider'));
    }

}
