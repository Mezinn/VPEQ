<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Data;

/**
 * SearchData represents the model behind the search form of `app\models\Data`.
 */
class SearchData extends Data
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'quality_state', 'elapsed_time', 'productivity_level', 'difficulty_level', 'quality_index'], 'integer'],
            [['city', 'company', 'nsp', 'created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Data::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'quality_state' => $this->quality_state,
            'elapsed_time' => $this->elapsed_time,
            'productivity_level' => $this->productivity_level,
            'difficulty_level' => $this->difficulty_level,
            'quality_index' => $this->quality_index,
            'created' => $this->created,
            'updated' => $this->updated,
        ]);

        $query->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'company', $this->company])
            ->andFilterWhere(['like', 'nsp', $this->nsp]);

        return $dataProvider;
    }
}
