<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coefficients".
 *
 * @property int $id
 * @property string $value
 */
class Coefficients extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'coefficients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['value'], 'required'],
            [['value'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'value' => 'Значення',
        ];
    }

}
