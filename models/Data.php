<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use app\models\Coefficients;
use app\models\EstimatesIntersection;

/**
 * This is the model class for table "data".
 *
 * @property int $id
 * @property string $city
 * @property string $company
 * @property string $nsp
 * @property int $quality_state
 * @property int $elapsed_time
 * @property int $productivity_level
 * @property int $difficulty_level
 * @property int $quality_index
 * @property string $created
 * @property string $updated
 */
class Data extends ActiveRecord {

    public $coefficients;
    public $evaluations;
    public $self_evaluations;
    public $estimates = [];
    public $estimates_sum;
    public $estimates_substract;
    public $divided_polynomials = [];
    public $divided_polynomials_with_b = [];
    public $evaluations_sum;
    public $self_evaluations_sum;
    public $x_square;
    public $k;
    public $k1;


    public function __construct($config = array()) {
        parent::__construct($config);
        $this->on(ActiveRecord::EVENT_AFTER_FIND, [$this, 'a_find']);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['city', 'company', 'nsp'], 'required'],
            [['quality_state', 'elapsed_time', 'productivity_level', 'difficulty_level', 'quality_index'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['city', 'company', 'nsp'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'city' => 'Місто',
            'company' => 'Компанія',
            'nsp' => 'П.І.П.',
            'quality_state' => 'Кваліфікаційний розряд',
            'elapsed_time' => 'Час витрачений на підготовку верстата ЧПУ(хв)',
            'productivity_level' => 'Рівень продуктивності зазначеного верстата з ЧПК',
            'difficulty_level' => 'Рівень складності програмного комплексу виготовлення деталі',
            'quality_index' => 'Індекс якості виготовлення деталі',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    public function a_find() {
        $this->coefficients = ArrayHelper::getColumn(Coefficients::find()->all(), function($model) {
                    return $model->value;
                });
        $this->evaluations = ArrayHelper::getColumn(Evaluation::findAll(['data_id' => $this->id]), function($model) {
                    return $model->value;
                });
        $this->self_evaluations = ArrayHelper::getColumn(SelfEvaluation::findAll(['data_id' => $this->id]), function($model) {
                    return $model->value;
                });
        $this->evaluations_sum = array_sum($this->evaluations);
        $this->self_evaluations_sum = array_sum($this->self_evaluations);
        foreach ($this->evaluations as $index => $evaluation) {
            array_push($this->estimates, new EstimatesIntersection(['evaluation' => $this->evaluations[$index], 'self_evaluation' => $this->self_evaluations[$index]]));
        }
        $this->estimates_sum = ArrayHelper::getColumn($this->estimates, function($model) {
                    return $model->evaluation + $model->self_evaluation;
                });

        $this->estimates_substract = ArrayHelper::getColumn($this->estimates, function($model) {
                    return $model->evaluation - $model->self_evaluation;
                });
        foreach ($this->estimates_substract as $index => $value) {
            array_push($this->divided_polynomials, pow($value, 2) / $this->estimates_sum[$index]);
        }
        foreach ($this->divided_polynomials as $index => $value) {
            array_push($this->divided_polynomials_with_b, $value * $this->coefficients[$index]);
        }
        $this->x_square = round(array_sum($this->divided_polynomials_with_b), 2);
        $this->k = round((9 * $this->evaluations_sum) / (10 + 3 * $this->x_square) + 0.5, 2);
    }

}
