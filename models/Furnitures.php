<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "furnitures".
 *
 * @property int $uid
 * @property string $name
 * @property string $price
 * @property int $type_uid
 */
class Furnitures extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'furnitures';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','price','type_uid'],'required'],
            [['price'], 'number'],
            [['type_uid'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Uid',
            'name' => 'Назва',
            'price' => 'Ціна',
            'type_uid' => 'Тип',
        ];
    }
}
