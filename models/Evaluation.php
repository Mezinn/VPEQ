<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "evaluation".
 *
 * @property int $data_id
 * @property string $value
 * @property int $internal_index
 */
class Evaluation extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'evaluation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['data_id', 'value'], 'required'],
            [['data_id', 'internal_index'], 'integer'],
            [['value'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'data_id' => 'Data ID',
            'value' => 'Експертна оцінка',
            'internal_index' => 'Internal Index',
        ];
    }

    public static function primaryKey() {
        return ['data_id', 'value', 'internal_index'];
    }

}
