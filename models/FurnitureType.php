<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "furniture_type".
 *
 * @property int $uid
 * @property string $name
 */
class FurnitureType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'furniture_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'],'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Uid',
            'name' => 'Назва',
        ];
    }
    
    public static function get_list(){
        return \yii\helpers\ArrayHelper::map(self::find()->all(),'uid','name');
    }
}
