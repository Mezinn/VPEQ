<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Coefficients */

$this->title = 'Додати коефіцієнт:';
$this->params['breadcrumbs'][] = ['label' => 'Коефіцієнти', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coefficients-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
