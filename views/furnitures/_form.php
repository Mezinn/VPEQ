<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\FurnitureType;

/* @var $this yii\web\View */
/* @var $model app\models\Furnitures */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="furnitures-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_uid')->dropDownList(FurnitureType::get_list()) ?>

    <div class="form-group">
        <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
