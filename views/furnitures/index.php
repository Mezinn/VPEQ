<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SearchFurnitures */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Меблі';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="furnitures-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Додати', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary'=>'',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'price',
            [
              'attribute'=>'type_uid',
               'filter' => app\models\FurnitureType::get_list(), 
              'value'=>function($model){
                $item = app\models\FurnitureType::findOne($model->type_uid);
                return ($item)?$item->name:"";
              }
            ],
            ['class' => 'yii\grid\ActionColumn','template'=>'{update}{delete}'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
