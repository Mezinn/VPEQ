<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Furnitures */

$this->title = 'Додати';
$this->params['breadcrumbs'][] = ['label' => 'Furnitures', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="furnitures-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
