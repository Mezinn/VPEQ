<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FurnitureType */

$this->title = 'Додати';
$this->params['breadcrumbs'][] = ['label' => 'Furniture Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="furniture-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
