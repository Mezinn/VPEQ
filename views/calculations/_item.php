
<?php

use yii\grid\GridView;
use app\models\SelfEvaluation;
use yii\grid\DataColumn;
use yii\data\ArrayDataProvider;

$dataProvider = new ArrayDataProvider([
    'allModels' => SelfEvaluation::findAll(['data_id' => $model->id])
        ])
?>

<?= $model->nsp ?>
