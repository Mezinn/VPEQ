<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Data */

$this->title = 'Оновлення даних: ';
$this->params['breadcrumbs'][] = ['label' => 'Дані', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Оновити';
?>
<div class="data-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model', 'evaluationDataProvider', 'selfEvaluationDataProvider')) ?>

</div>
