<?php

$labelIndex = $index + 1;
?>
<?= $form->field($model, "[$index]value")->textInput()->label($model->getAttributeLabel('value') . " № ($labelIndex)"); ?>
<?= $form->field($model, "[$index]internal_index")->hiddenInput(['value' => $index])->label(false); ?>