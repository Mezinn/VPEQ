<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SearchData */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Дані';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?= Html::a('Додаты', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nsp',
//            'city',
            'company',
            'quality_state',
            //'elapsed_time:datetime',
            //'productivity_level',
            //'difficulty_level',
            //'quality_index',
            //'created',
            //'updated',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}'],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
