<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Data */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nsp')->textInput(['maxlength' => true]) ?>

    <div class='row'>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'company')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class='row'>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'quality_state')->textInput() ?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'elapsed_time')->textInput() ?>
        </div>
    </div>

    <?=
    ListView::widget([
        'dataProvider' => $selfEvaluationDataProvider,
        'itemView' => '_eform',
        'viewParams' => compact('form'),
        'summary' => '',
    ])
    ?>

    <?=
    ListView::widget([
        'dataProvider' => $evaluationDataProvider,
        'itemView' => '_eform',
        'viewParams' => compact('form'),
        'summary' => '',
    ])
    ?>

    <div class='row'>
        <div class='col-md-6 col-sm-6 col-xs-12'>
            <?= $form->field($model, 'productivity_level')->textInput() ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12'>
            <?= $form->field($model, 'difficulty_level')->textInput() ?>
        </div>
    </div>
    <?= $form->field($model, 'quality_index')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
