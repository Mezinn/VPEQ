<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Data */

$this->title = 'Додати дані';
$this->params['breadcrumbs'][] = ['label' => 'Дані', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model', 'evaluationDataProvider', 'selfEvaluationDataProvider')) ?>

</div>
